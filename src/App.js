/* @flow */

import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';
import Quizz from './UI/Quizz';
import QuizzView from './UI/QuizzView';
import './App.css';
import quizz from './quizz.json';
import theme from './theme.json';

export default class App extends Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <Quizz quizz={QuizzView.fromJSON(quizz)} />
            </ThemeProvider>
        );
    }
}
