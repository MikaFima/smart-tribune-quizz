/* @flow */

import * as React from 'react';
import styled from 'styled-components';
import QuestionView from './QuestionView';
import type { AnswerType } from './QuestionView';

const Box = styled.div`
    padding: ${({ theme }) => theme.margins.m2};
    box-shadow: ${({ theme }) => theme.boxShadow};
`;

const Title = styled.h3`
    padding: ${({ theme }) => theme.margins.m1};
    text-align: center;
`;

const Image = styled.img`
    width: 100%;
    height: auto;
`;

const AnswerLink = styled.a`
    display: block;
    padding: ${({ theme }) => theme.margins.m1};
    text-align: center;
    box-shadow: ${({ theme }) => theme.boxShadow};
    border-radius: ${({ theme }) => theme.borderRadius};

    &:hover {
        background-color: silver;
    }
`;

type Props = {
    question: QuestionView,
};

export default class Question extends React.Component<Props> {
    renderImage() {
        const { question } = this.props;

        if (!question.image) {
            return null;
        }

        return <Image src={question.image} />;
    }

    render() {
        const { question } = this.props;

        return (
            <Box>
                {this.renderImage()}
                <Title>{question.text}</Title>
                {question.answers.map((answer: AnswerType) => (
                    <AnswerLink key={`Answer${answer.text}`}>{answer.text}</AnswerLink>
                ))}
            </Box>
        );
    }
}
