/* @flow */

import * as React from 'react';
import QuizzView from './QuizzView';
import styled from 'styled-components';
import Question from './Question';

const Section = styled.section`
    font-family: ${({ theme }) => theme.textFontFamily};
    width: 100%;
    max-width: 300px;
    margin: 0 auto;
`;

const ButtonsGrid = styled.div`
    display: flex;
    flex-direction: row;
    margin-bottom: ${({ theme }) => theme.margins.m2};
`;

const ButtonCell = styled.div`
    flex: 1;

    &:not(:last-child) {
        padding-right: ${({ theme }) => theme.margins.m1};
    }

    &:not(:first-child) {
        padding-left: ${({ theme }) => theme.margins.m1};
    }
`;

const Button = styled.button`
    cursor: ${({ active }) => (active ? 'auto' : 'not-allowed')};
    border-radius: ${({ theme }) => theme.borderRadius};
    width: 100%;
    background: ${({ active }) => (active ? 'white' : 'silver')};
    color: ${({ active }) => (active ? 'black' : 'gray')};
    box-shadow: ${({ theme }) => theme.boxShadow};
    border: none;
    padding: ${({ theme }) => theme.margins.m1};
`;

const PreviousButton = Button.extend``;

const NextButton = Button.extend``;

const Title = styled.h1`
    text-align: center;
    padding: ${({ theme }) => theme.margins.m2};
    font-weight: normal;
    color: ${({ theme }) => theme.primaryColor};
    font-family: ${({ theme }) => theme.titleFontFamily};
`;

type Props = {
    quizz: QuizzView,
};

type State = {
    questionIndex: number,
};

export default class Quizz extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            questionIndex: 0,
        };
    }

    onPreviousClick() {
        const { questionIndex } = this.state;

        this.setState({
            questionIndex: Math.max(questionIndex - 1, 0),
        });
    }

    onNextClick() {
        const { quizz } = this.props;
        const { questionIndex } = this.state;

        this.setState({
            questionIndex: Math.min(questionIndex + 1, quizz.questions.length - 1),
        });
    }

    render() {
        const { quizz } = this.props;
        const { questionIndex } = this.state;

        return (
            <Section>
                <Title>{quizz.title}</Title>
                <ButtonsGrid>
                    <ButtonCell>
                        <PreviousButton
                            active={questionIndex > 0}
                            onClick={this.onPreviousClick.bind(this)}
                        >
                            précédent
                        </PreviousButton>
                    </ButtonCell>
                    <ButtonCell>
                        <NextButton
                            active={questionIndex < quizz.questions.length - 1}
                            onClick={this.onNextClick.bind(this)}
                        >
                            suivant
                        </NextButton>
                    </ButtonCell>
                </ButtonsGrid>
                <Question question={quizz.questions[questionIndex]} />
            </Section>
        );
    }
}
