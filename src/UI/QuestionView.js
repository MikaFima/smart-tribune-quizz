/* @flow */

export type QuestionParameters = {
    display_correction: boolean,
    multiple_choices: boolean,
};

export type AnswerType = {
    text: string,
    isCorrect: boolean,
};

export default class QuestionView {
    uuid: string;
    text: string;
    displayCorrection: boolean;
    multipleChoice: boolean;
    answers: Array<AnswerType>;
    image: ?string;

    constructor(
        uuid: string,
        text: string,
        displayCorrection: boolean,
        multipleChoice: boolean,
        answers: Array<AnswerType>,
        image: ?string,
    ) {
        this.uuid = uuid;
        this.text = text;
        this.displayCorrection = displayCorrection;
        this.multipleChoice = multipleChoice;
        this.answers = answers;
        this.image = image;

        Object.freeze(this);
    }

    static fromJSON(json: object, defaultParameters: QuestionParameters): QuestionView {
        return new QuestionView(
            json.uuid,
            json.text,
            json.params && json.params.display_correction !== undefined
                ? json.params.display_correction
                : defaultParameters.display_correction,
            json.params && json.params.multiple_choices !== undefined
                ? json.params.multiple_choices
                : defaultParameters.multiple_choices,
            json.answers.map((answer: { text: string, is_correct: boolean }) => ({
                text: answer.text,
                isCorrect: answer.is_correct,
            })),
            json.image,
        );
    }
}
