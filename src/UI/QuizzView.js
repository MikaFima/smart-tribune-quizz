/* @flow */

import QuestionView from './QuestionView';

export default class QuizzView {
    title: string;
    questions: Array<QuestionView>;

    constructor(title: string, questions: Array<QuestionView>) {
        this.title = title;
        this.questions = questions;

        Object.freeze(this);
    }

    static fromJSON(input: object): QuizzView {
        return new QuizzView(
            input.title,
            input.questions.map((question: object) =>
                QuestionView.fromJSON(question, input.default_params),
            ),
        );
    }
}
