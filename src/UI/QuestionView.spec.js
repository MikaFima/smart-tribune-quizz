/* @flow */

import QuestionView from './QuestionView';

describe('QuestionView', () => {
    it('should be instanciated', () => {
        const sut = new QuestionView('uuid', 'text', true, false, [], 'http://image');

        expect(sut.uuid).toBe('uuid');
        expect(sut.text).toBe('text');
        expect(sut.displayCorrection).toBe(true);
        expect(sut.multipleChoice).toBe(false);
        expect(sut.answers).toEqual([]);
        expect(sut.image).toBe('http://image');
    });

    describe('should be instanciated from json', () => {
        const defaultParameters = {
            display_correction: true,
            multiple_choices: true,
        };

        const basicPayload = {
            uuid: 'ec73ef69-4055-4929-8557-65be4e9e1438',
            text: 'Question 01',
            answers: [
                {
                    text: 'Réponse 00',
                    is_correct: false,
                },
                {
                    text: 'Réponse 01',
                    is_correct: true,
                },
            ],
        };

        const fullPayload = Object.assign({}, basicPayload, {
            params: {
                display_correction: false,
                multiple_choices: false,
            },
            image: 'http://lorempicsum.com/futurama/255/200/2',
        });

        it('with basic payload', () => {
            const sut = QuestionView.fromJSON(basicPayload, defaultParameters);

            expect(sut.uuid).toBe('ec73ef69-4055-4929-8557-65be4e9e1438');
            expect(sut.text).toBe('Question 01');
            expect(sut.displayCorrection).toBe(true);
            expect(sut.multipleChoice).toBe(true);
            expect(sut.answers).toEqual([
                {
                    text: 'Réponse 00',
                    isCorrect: false,
                },
                {
                    text: 'Réponse 01',
                    isCorrect: true,
                },
            ]);
            expect(sut.image).toBe(undefined);
        });

        it('with full payload', () => {
            const sut = QuestionView.fromJSON(fullPayload, defaultParameters);

            expect(sut.displayCorrection).toBe(false);
            expect(sut.multipleChoice).toBe(false);
            expect(sut.image).toBe('http://lorempicsum.com/futurama/255/200/2');
        });
    });
});
