/* @flow */

import QuizzView from './QuizzView';
import QuestionView from './QuestionView';

jest.mock('./QuestionView');

describe('QuizzView', () => {
    it('should be instanciated', () => {
        const questions = new QuestionView();

        const sut = new QuizzView('title', questions);

        expect(sut.title).toEqual('title');
        expect(sut.questions).toEqual(questions);
    });

    it('should be instanciated from json', () => {
        const json = {
            title: 'Mon Super Quiz',
            default_params: {
                display_correction: false,
                multiple_choices: true,
            },
            questions: [
                {
                    test: 'question1',
                },
                {
                    test: 'question2',
                },
                {
                    test: 'question3',
                },
            ],
        };

        const sut = QuizzView.fromJSON(json);

        expect(sut.title).toEqual('Mon Super Quiz');
        expect(QuestionView.fromJSON).toHaveBeenCalledTimes(3);
        expect(QuestionView.fromJSON).toHaveBeenCalledWith(json.questions[0], json.default_params);
        expect(QuestionView.fromJSON).toHaveBeenCalledWith(json.questions[1], json.default_params);
        expect(QuestionView.fromJSON).toHaveBeenCalledWith(json.questions[2], json.default_params);
    });
});
